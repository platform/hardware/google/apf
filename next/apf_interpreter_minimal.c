typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;

#define NULL ((void*)0)

typedef enum { false, true } bool;
#include "../apf_defs.h"
#include "../apf_utils.h"
#include "../apf_dns.h"
#include "../apf_checksum.h"
